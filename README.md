# DeliverMe
DeliverMe is a web service allowing for delivery of any good, from any store in your local area by our drivers.

---
### Getting started
1. The first step is to have the write software installed. You will need the following
- [Git](https://git-scm.com/)
- [Node.js - I use node version 8.11.3 and npm version 5.6.0](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/lang/en/)
- You may also need sourcetree, to help make git usage easier if you dont like the command prompt
- You could also use [HeidiSQL](https://www.heidisql.com/) to let you interact with the database structure.
- Possibly a MYSQL driver.

2. Secondly, you must clone the repository. You can run the command to do this - assuming you have privileges.
    ```sh
    $ git clone https://gitlab.com/DeliverMe/website.git
    ```
    This should generate a file structure something like this:
    ```
    DeliverMe
    |
    │   start,bat
    |   README.md
    |   LICENSE
    |
    └───/frontend
    │   │   /config
    │   │   /public
    |   |   ...
    │   │
    │   └───/src
    │       │   index.js
    │       │   store.js
    │       │   ...
    │   
    └───/backend
        │   database.js
        │   index.js
        |   ... And some other folders containing the API and backend Logic
    ```
    
3. Now this is complete you should go to the route directory and run the following commands in order. They may take a few minutes to complete if the project dependencies arent already cached on your system.
    ```sh
    $ cd backend
    $ yarn
    $ cd ../
    $ cd frontend
    $ yarn
    ```
    This should create all project dependencies you need for both the frontend and backend.
    
4. Now you are able to start the project and use the development environment. This can be done by going into the root folder (above frontend and backend directories) and clicking the **start.bat** file.
---
### How to write new code.
The following section outlines details on how to get started writing new code for the project, **please read carefully**.

- You must adhere to the git structure we use, you will not be able to commit directly to the master branch and should therefore ask for a new branch for yourself to be created.

- Once you have a new branch for yourself created, or you start using one of the default ones such as "DEV" you can checkout the branch in git by running the command ```$ git checkout <branch name>```

- You can now write new code to the repo, you may find the information in the following section useful in learning the code base structure.

- You can then commit your code, using the command ```$ git commit -m "Updated some code" -a``` - This command may require you to add the files first, using the command ```git add .``` to add all files or ```$ git add <file name>``` to add individual files.

- And then push the code using the command ```$ git push```

- Another useful command may be ```$git status``` to see information regarding your code base and the repo itself.
    


