import axios from "axios";
import {browserHistory} from "react-router";


//Register user
export function userRegisterRequest() {
    return {type: "USER_REGISTER_REQUEST"}
}

export function userRegisterSuccess(user) {
    return {type: "USER_REGISTER_SUCCESS", data: user}
}

export function userRegisterFailure(err) {
    return {type: "USER_REGISTER_FAILURE", err}
}

export function updateUser(message) {
    return {type: "UPDATE_USER", message}
}


export function registerUser(forename, surname, email, password) {
    return dispatch => {
        dispatch(userRegisterRequest());
        return axios.post("http://localhost:7033/user/register", {forename, surname, email, password}).then((response)=> {
            if(response.data.ok === true) {
                dispatch(updateUser("Please go to your email address and verify your account. The email may have ended up in your spam box, check that!"));
            } else {
                dispatch(userRegisterFailure(response.data.error));
            }  
        }).catch((err) => {
            dispatch(userRegisterFailure(err));
        });
    }
}


//Login user.

export function userLoginRequest() {
    return {type: "USER_LOGIN_REQUEST"}
}

export function userLoginSuccess(username, token) {
    return {type: "USER_LOGIN_SUCCESS", username, token}
}

export function userLoginFailure(err) {
    return {type: "USER_LOGIN_FAILURE", err}
}

export function loginUser(email, password) {
    return dispatch => {
        dispatch(userRegisterRequest());
        return axios.post("http://localhost:7033/user/login", {email, password}).then((response)=> {
            if(response.data.ok === true) {
                localStorage.setItem("token", response.data.token);
                dispatch(userLoginSuccess(response.data.username, response.data.token));
                browserHistory.push("/");
            } else {
                dispatch(userLoginFailure(response.data.error));
            }  
        }).catch((err) => {
            dispatch(userLoginFailure(err));
        });
    }
}

//Logout user

export function userLogoutRequest() {
    return {type: "USER_LOGOUT"}
}
export function logoutUser() {
    return dispatch => {
        localStorage.removeItem("token");
        dispatch(userLogoutRequest());
    }
    
}

//Clear messages
export function clearMessages() {
    return dispatch => {
        dispatch(userLoginFailure(""));
        dispatch(updateUser(""));
        dispatch(userRegisterFailure(""));
    }
}