import React, { Component } from "react";


export default class BecomeADriver extends Component {

    constructor() {
        super();
        this.state = {
            modalIsOpen: false,
        };
    }

    closeModal(){
        this.props.toClose(false);
    }


    render() {
        return (
            <div className="become-a-driver-modal">
                <div className="top">
                    <div className="close-button" onClick={()=> this.closeModal()}>
                        <i className="fas fa-times-circle"></i>
                    </div>
                    <h2>Become a Driver</h2>
                </div>
                <div className="body">
                    <p>Driving is convenient, has good pay and allows you to be part of the driver community.</p>
                    <h4>Benefits</h4>
                    <ul>
                        <li>Good pay - £600/hr</li>
                        <li>Drivers insurance, protecting your goods</li>
                        <li>Can work wherever you want, whenever you want. *</li>
                    </ul>
                    <a href="/driver/register">Apply now</a>
                    <small>Terms and conditions apply, read the terms page for more information. Benefits tagged "*" have timed restrictions and may not be possible for all drivers upon sign up.</small>
                </div>
            </div>
        );
    }
}
