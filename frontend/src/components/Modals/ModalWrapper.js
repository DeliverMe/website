import React, { Component } from "react";
import Modal from "react-modal";



// const customStyles = {
//     content : {
//         width: "28%",
//         height: "600px",
//         margin: "auto auto",

//         padding: "16px",
//         zIndex: "100000",
//     }
// };
  



// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');

export default class ModalWrapper extends Component {
    constructor() {
        super();
        this.state = {
            modalIsOpen: false,
            topOffset: 0,
        };
    }


    closeModal(){
        this.props.changeState(false);
    }


    render() {
        return (
            <div>
                <Modal overlayClassName="modal-outer" className="modal-content" isOpen={this.props.modalIsOpen} style={{overlay: {top: window.scrollY + "px", height: window.innerHeight + "px"}}} onRequestClose={()=> this.closeModal()} contentLabel="Become a Rider" >
                    <div className="modal-inner">
                        {this.props.children}
                    </div>
                </Modal>
            </div>
        );
    }
}
