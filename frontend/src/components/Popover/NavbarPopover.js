import React, { Component } from "react";
import { browserHistory } from "react-router"

export default class NavbarPopover extends Component {

    //https://medium.com/@pitipatdop/little-neat-trick-to-capture-click-outside-react-component-5604830beb7f use this to detect outside click or the on blur.
    render() {
        return (
            <div className="popover" ref={node => this.node = node}>
                <ul className={this.props.visibile ? "visibile" : "invis"}>
                    <li onClick={()=> browserHistory.push("/account")}>Account</li>
                    <li onClick={()=> browserHistory.push("user-auth/logout")}>Log out</li>
                </ul>
            </div>
        );
    }
}