import React from "react";

const Category = ({type}) => {
    if(type === "General") {
        return (
            <span><i className="fas fa-shopping-cart"></i>General</span>
        );
    } else if(type === "Food") {
        return (
            <span><i className="fas fa-utensils"></i>Food</span>
        );
    } else if(type === "Alcohol") {
        return (
            <span><i className="fas fa-beer"></i>Alcohol</span>
        );
    } else {
        return (
            <span><i className="fas fa-shopping-cart"></i>General</span>
        );
    }

    
    
};

export default Category;