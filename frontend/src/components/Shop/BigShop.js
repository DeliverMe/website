import React, {Component} from 'react';
import { browserHistory } from "react-router";


export default class BigShop extends Component {

    render() {
        return (
            <div onClick={()=> browserHistory.push("/store/" + this.props.area + "/" + this.props.name)}className={this.props.offset === true ? "col-xs-5 col-xs-offset-2 big-shop" : "col-xs-5 big-shop"}>
                <div className="top-row">
                    <h4>{this.props.title}</h4>
                    <p>{this.props.location}</p>
                </div>
                <div className="image-holder">
                    <img src={"/shops/" + this.props.name + "-" + this.props.area + ".jpg"} alt="Shop Pic" />
                    <p className="popular">Popular</p>
                    <p className="wait-time"><i className="far fa-clock"></i>{this.props.waitTime} Mins</p>
                    <div className="dropdown-button"><i className="fas fa-caret-down"></i></div>
                </div>
                
            </div>
        );
    }
}
