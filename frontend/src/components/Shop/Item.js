import React, { Component } from "react";

export default class Item extends Component {

    render() {
        return(
            <div className="shop-item">
                <h4>{this.props.name}</h4>
                <p>£{this.props.price}</p>
            </div>
        );
    }
}