import React, {Component} from 'react';
import { browserHistory } from "react-router";

export default class Footer extends Component {

    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-4">
                            <h5>More about DeliverMe</h5>
                            <ul>
                                <li>About us</li>
                                <li>News</li>
                                <li>Careers</li>
                                <li onClick={()=> browserHistory.push("/store/auth")} >Store signup</li>
                                <li onClick={()=> browserHistory.push("/admin/dashboard")} >Admin</li>
                            </ul>
                        </div>
                        <div className="col-xs-4">
                            <h5>Legal</h5>
                            <ul>
                                <li onClick={()=> browserHistory.push("/terms")} >Terms and conditions</li>
                                <li>Privacy</li>
                                <li>Cookies</li>
                                <li>Modern Slavery Statement</li>
                            </ul>
                        </div>
                        <div className="col-xs-4">
                            <h5>Help</h5>
                            <ul>
                                <li>Contact</li>
                                <li>FAQ</li>
                            </ul>
                        </div>
                    </div>
                    <div className="row social-row">
                        <i className="fab fa-facebook-f"></i>
                        <i className="fab fa-twitter"></i>
                        <i className="fab fa-instagram"></i>
                        <p style={{justifyContent: "flex-end", marginLeft: "auto"}}>Redesign made by Thomas Ludlow Copyright of: © {new Date().getFullYear()} Deliveroo</p>
                    </div>
                </div>
            </footer>
        );
    }
}
