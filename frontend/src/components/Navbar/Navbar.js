import React, { Component } from "react";
import { Link } from "react-router";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from '../../actions/actionCreators';

import NavbarPopover from "../Popover/NavbarPopover";

class Navbar extends Component {

    constructor(props) {
        super();
        this.state = {
            navbarPopoverState: false,
        };
    }
    toggleNavbarPopover() {
        this.state.navbarPopoverState ? this.setState({navbarPopoverState: false}) : this.setState({navbarPopoverState: true});
    }

    profileArea() {
        if(this.props.user.loggedIn === true) {
            return (
                
                <ul className="nav navbar-nav navbar-right">
                    <li className="hidden"><a href="#page-top">Page Top</a></li>
                    <li><a href="/">Home</a></li>
                    <li onClick={()=> this.toggleNavbarPopover()}>
                        <a className="profile-name popover-holder">{this.props.user.username}<i className="fas fa-caret-down"></i><NavbarPopover visibile={this.state.navbarPopoverState} toClose={false} /></a>
                    </li>
                </ul>
            );
        } else {
            return (
                <ul className="nav navbar-nav navbar-right">
                    <li className="hidden"><a href="#page-top">Page Top</a></li>
                    <li><a href="/">Home</a></li>
                    <li><a href="/user-auth/login">Sign In</a></li>
                    <li><a href="/user-auth/register">Register</a></li>
                </ul>
            );
        }
    }

    render() {
        return (
            <div>
                <nav id="mainNav" className="navbar navbar-default navbar-custom">
                    <div className="container">
                        <div className="navbar-header page-scroll">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <div className="navbar-brand-container">
                                <Link to="/"><img src="/logo.png" alt="deliverme logo"/></Link>
                            </div>
                        </div>
                        <span className="collapse navbar-collapse clearfix" id="bs-example-navbar-collapse-1">
                            {this.profileArea()}
                        </span>
                    </div>
                </nav>
            </div>
        );
    }
}

function mapStateToProps(state) {
	return {user: state.user};
}


export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

let NavbarClass = connect(mapStateToProps, mapDispatchToProps)(Navbar);

export default NavbarClass;
