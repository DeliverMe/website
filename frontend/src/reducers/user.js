function user(state = {}, action) {
    switch (action.type) {
        case 'USER_REGISTER_REQUEST':
            return {...state, loading: true, errors: ""}
        case 'USER_REGISTER_SUCCESS':
            return {...state, loading: false, errors: ""}
        case 'USER_REGISTER_FAILURE':
            return {...state, loading: false, errors: action.err}

        case 'USER_LOGIN_REQUEST':
            return {...state, loading: true, errors: ""}
        case 'USER_LOGIN_SUCCESS':
            return {...state, loading: false, errors: "", loggedIn: true, username: action.username, token: action.token}
        case 'USER_LOGIN_FAILURE':
            return {...state, loading: false, errors: action.err}

        case 'UPDATE_USER':
            return {...state, message: action.message, loading: false}
        case 'USER_LOGOUT':
            return {...state, loggedIn: false, token: "", username: "", errors: ""}
        default:
            return state;
    }
}

export default user;