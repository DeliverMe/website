/*
  Import Dependencies
*/
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute} from 'react-router'
import 'babel-polyfill';

/* Import CSS */
//eslint-disable-next-line
import css from './styles/main.scss';

/*
  Import Components
*/
import App from './components/App';

/*
  Import Views
*/
import Home from './views/Home';
import FourOFour from "./views/FourOFour";
import UserAuth from "./views/UserAuth";
import Area from "./views/Area";
import Account from "./views/Account";
import Shop from "./views/Store/Shop";
import Checkout from "./views/Checkout";
import DriverPage from "./views/Driver/Driver";
import DriverRegister from "./views/Driver/DriverRegister";

//Store
import StoreAuth from "./views/Store/StoreAuth";


//Admin
import Dashboard from "./views/Admin/Dashboard";


/* Import our data store */
import store, {history} from './store';

//Auth verification routes
import verifyAuth from "./views/VerifyAuth";


/*
  Rendering
  This is where we hook up the Store with our actual component and the router
*/

//TODO MAKE A LEADERBOARD FOR PERSON WHO ORDERS MOST IN A SET TIME FRAME AND WHO DELIVERS THE MOST IN A SET TIME FRAME.
render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={Home} />
                <Route path="/user-auth/:type" component={UserAuth} />
                <Route path="/store/auth" component={StoreAuth} />
                <Route path="/admin/dashboard" component={Dashboard}/>
                <Route path="/area/:where" component={Area}/>
                <Route path="/store/:where/:name" component={Shop} />
            </Route>

            <Route path="/driver" component={verifyAuth}>
                <IndexRoute component={DriverPage} />
                <Route path="/driver/register" component={DriverRegister} />
            </Route>

            <Route path="/" component={verifyAuth}>
                <Route path="/checkout" component={Checkout} />
                <Route path="/account" component={Account} />
            </Route>

            <Route path="/*" component={FourOFour} />
        </Router>
    </Provider>,
document.getElementById('root'));
