import React, {Component} from "react";
import {Link, browserHistory} from "react-router";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from '../actions/actionCreators';


import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";

class UserAuth extends Component {

    constructor(props) {
        super();
        this.state = {
            errors: "",
        }
    }

    componentWillMount() {
        this.props.clearMessages();
        if(this.props.params.type === "logout") {
            this.props.logoutUser();
            browserHistory.push("/");
        }
    }


    handleLogin(e) {
        e.preventDefault();
        this.setState({loading: true, errors: ""});
        let emailAddress = this.refs.loginEmail.value;
        let password = this.refs.loginPassword.value;

        //Login is valid, lets try and send an axios request
        this.props.loginUser(emailAddress, password);
    }

    handleRegister(e) {
        e.preventDefault();
        this.setState({loading: true, errors: ""});
        let firstname = this.refs.registerFirstname.value;
        let surname = this.refs.registerSurname.value;
        let emailAddress = this.refs.registerEmail.value;
        let password = this.refs.registerPassword.value;
        let confirmPassword = this.refs.registerPasswordConfirm.value;

        if(password !== confirmPassword) {
            this.setState({loading: false, errors: "Your password's do not match."});
            return;
        }

        //All is good, send the register request.
        this.props.registerUser(firstname, surname, emailAddress, password);

    }

    render() {
        if(this.props.params.type === "login") {
            return (
                <div className="user-auth">
                    <div className="wrapper">
                        <Navbar />
                        <section>
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-5">
                                        <h3>Log in to your DeliverMe account</h3>
                                        <p>Don't have an account? <Link to="/user-auth/register">Register</Link></p>
                                        <form onSubmit={(e)=> this.handleLogin(e)}>
                                            <p>Email address</p>
                                            <input type="text" placeholder="randomemail@gmail.com" ref="loginEmail" required/>

                                            <p>Password</p>
                                            <input type="password" ref="loginPassword" required/>

                                            {this.props.user.errors.length > 0 ? <p className="error">{this.props.user.errors}</p> : ""}
                                            {this.state.errors.length > 0 ? <p className="error">{this.state.errors}</p> : ""}
                                            <button type="submit" className="user-auth-submit">{this.props.user.loading ? <i className="fas fa-spinner"></i> : <span>Log In <i className="fas fa-arrow-right gap-right"></i></span>}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <Footer />
                </div>
            );
        } if(this.props.params.type === "register") {
            return (
                <div className="user-auth">
                    <div className="wrapper">
                        <Navbar />
                        <section>
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-5">
                                        <h3>Register an account with DeliverMe</h3>
                                        <p>Already have an account? <Link to="/user-auth/login">Log in</Link></p>
                                        <form onSubmit={(e)=> this.handleRegister(e)}>
                                            <p>Forename (firstname)</p>
                                            <input type="text"  ref="registerFirstname" required/>

                                            <p>Surname (lastname)</p>
                                            <input type="text" ref="registerSurname" required/>

                                            <p>Email address</p>
                                            <input type="text" placeholder="randomemail@gmail.com" ref="registerEmail" required/>

                                            <p>Password</p>
                                            <input type="password" ref="registerPassword" required/>

                                            <p>Confirm Password</p>
                                            <input type="password" ref="registerPasswordConfirm" required/>

                                            <p>By clicking register you agree to our <Link to="/terms">terms &amp; conditions</Link></p>
                                            <br/>
                                            {this.props.user.message.length > 0 ? <p className="message">{this.props.user.message}</p> : ""}
                                            {this.props.user.errors.length > 0 ? <p className="error">{this.props.user.errors}</p> : ""}
                                            {this.state.errors.length > 0 ? <p className="error">{this.state.errors}</p> : ""}
                                            <button type="submit" className="user-auth-submit">{this.props.user.loading ? <i className="fas fa-spinner"></i> : <span>Register <i className="fas fa-arrow-right gap-right"></i></span>}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <Footer />
                </div>
            );
        }
        if(this.props.params.type === "logout") {
            return (
                <div className="user-auth">
                    <div className="wrapper">
                        <Navbar />
                        <section>
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-5">
                                        <h2>Logging out...</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <Footer />
                </div>
            );
        }
        if(this.props.params.type === "confirmed") {
            return (
                <div className="user-auth">
                    <div className="wrapper">
                        <Navbar />
                        <section>
                            <div className="container">
                                <h2>You have confirmed your account, you may now log in.</h2>
                                <Link className="site-button" to="/user-auth/login">Log in</Link>
                            </div>
                        </section>
                    </div>
                    <Footer />
                </div>
            );
        }
        else {
            return (
                <div className="user-auth">
                    <div className="wrapper">
                        <Navbar />
                        <section>
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-5">
                                        <h3>You have successfully verified your account.</h3>
                                        <p>You can now log into your account and use all of our features.</p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <Footer />
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
	return {user: state.user};
}


export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

let UserAuthClass = connect(mapStateToProps, mapDispatchToProps)(UserAuth);

export default UserAuthClass;
