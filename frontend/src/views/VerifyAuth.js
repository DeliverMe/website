import { Component } from "react";
import { browserHistory } from "react-router";
import {connect} from 'react-redux';

class VerifyAuth extends Component {

    componentWillMount() {
        if(!this.props.user.loggedIn) {
            browserHistory.push("/user-auth/login");
        }
    }
  
    render() {
        if(this.props.user.loggedIn) {
            return this.props.children
        } else {
            return false;
        }
    }

  }
  
function mapStateToProps(state) {
	return {user: state.user};
}


let VerifyAuthClass = connect(mapStateToProps)(VerifyAuth);

export default VerifyAuthClass;