import React, { Component } from "react";
import axios from "axios";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from '../../actions/actionCreators';

import Navbar from "../../components/Navbar/Navbar";
import { browserHistory } from "react-router";

class Dashboard extends Component {

    constructor(props) {
        super();
        this.state = {
            userLoading: true,
            userError: "",
            allUsers: [],
            generalCategory: false,
            foodCategory: false,
            alcoholCategory: false
        };
    }

    componentDidMount() {
        if (this.props.user !== undefined && this.props.user.loggedIn) {
            let token = this.props.user.token;
            console.log(token);
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}` 
        }
        //Check if admin.
        axios.post("http://localhost:7033/admin/verifyUser").then((response)=> {
            if(response.data.ok === false) {
                browserHistory.push("/");
            } else {
                axios.get("http://localhost:7033/admin/getAllUsers").then((response)=> {
                    console.log(response);
                    if(response.data.ok === false) {
                        this.setState({userLoading: false, userError: response.data.error});
                    } else {
                        this.setState({userLoading: false, allUsers: response.data.users});
                    }
                }).catch((err)=> {
                    this.setState({users: {loading: false, error: err}});
                });
            }
        });
    }

    renderUsers() {
        if(this.state.userLoading) {
            return (
                <p>Loading...</p>
            );
        } 
        if(this.state.userError.length > 0) {
            return (<p className="error">{this.state.userError}</p>);
        } else {
            return (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.allUsers.map((user, i)=> (
                        <tr key={i}>
                            <td><strong>{user.user_id}</strong></td>
                            <td>{user.email}</td>
                            <td>{user.forename} {user.surname}</td>
                            <td>{user.role}</td>
                            <td><i className="fas fa-pen"></i> <i className="fas fa-trash-alt"></i></td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            );
        }
    }

    categoryManager(cat) {
        if(cat === "general") {
            this.state.generalCategory ? this.setState({generalCategory: false}) : this.setState({generalCategory: true})
        }
        if(cat === "food") {
            this.state.foodCategory ? this.setState({foodCategory: false}) : this.setState({foodCategory: true})
        }
        if(cat === "alcohol") {
            this.state.alcoholCategory ? this.setState({alcoholCategory: false}) : this.setState({alcoholCategory: true})
        }
    }
    addStoreFormSubmit(e) {
        e.preventDefault();
        let name = this.refs.addStoreName.value;
        let address = this.refs.addStoreAddress.value;
        let openTime = this.refs.addStoreOpenTime.value;
        let closeTime = this.refs.addStoreCloseTime.value;
        //handle categories through component state
        let description = this.refs.addStoreDescription.value;

        console.log(name, address, openTime, closeTime, description);
    }

    render() {

//TODO MAKE THE ADMIN FORM LOOK MORE LIKE THE ADMIN DASHBOARD STYLE, MAKE TEXT INPUT PLACEHOLDERS SMALLER ETC.

        return(
            <div className="admin-dashboard">
                <div className="wrapper">
                    <Navbar />
                    <div className="container content">
                        <div className="top">
                            <h3>Admin Dashboard</h3>
                            <div className="warning">Be aware, actions made on this page are important and should not be done without consideration.</div>
                        </div>
                        <div className="row">
                            <div className="col-xs-8 area users">
                                <h4>Users <small>Ordered by newest (50 users only)</small></h4>
                                {this.renderUsers()}
                            </div>
                            <div className="col-xs-3 col-xs-offset-1 area log">
                                <h4>Log</h4>
                            </div>
                        </div>
                        <div className="row gap-top">
                            <div className="col-xs-4 area add-store">
                                <h4>Add Store</h4>
                                <form onSubmit={(e)=> this.addStoreFormSubmit(e)}>
                                    <br/>
                                    <div className="form-gap">
                                        <p>Name</p>
                                        <input type="text" name="" id="" placeholder="Sainsburys" ref="addStoreName" />
                                    </div>
                                    <div className="form-gap">
                                        <p>Address</p>
                                        <input type="text" name="" id="" placeholder="34 Leonard Avenue, SL4 69Q, Windsor" ref="addStoreAddress" />
                                    </div>
                                    <div className="time-manager">
                                        <div className="left">
                                            <p>Open Time</p>
                                            <input type="time" name="" id="" ref="addStoreOpenTime"/>
                                        </div>
                                        <div className="right">
                                            <p>Closing Time</p>
                                            <input type="time" name="" id="" ref="addStoreCloseTime"/>
                                        </div>
                                    </div>
                                    <div className="form-gap">
                                        <p>Categories</p>
                                        <div className="checkbox-area">
                                            <div className="check">
                                                <label className="label">
                                                    <input  className="label__checkbox" type="checkbox" />
                                                    <span className="label__text">
                                                        <span className="label__check" onClick={()=> this.categoryManager("general")}>
                                                            <p>General</p>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div className="check">
                                                <label className="label">
                                                    <input  className="label__checkbox" type="checkbox" />
                                                    <span className="label__text">
                                                        <span className="label__check" onClick={()=> this.categoryManager("food")}>
                                                            <p>Food</p>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div className="check">
                                                <label className="label">
                                                    <input  className="label__checkbox" type="checkbox" />
                                                    <span className="label__text">
                                                        <span className="label__check" onClick={()=> this.categoryManager("alcohol")}>
                                                            <p>Alcohol</p>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="form-gap description-area">
                                        <p>Description</p>
                                        <textarea name="" id="" cols="43" rows="10" ref="addStoreDescription"></textarea>
                                    </div>

                                    <button type="submit" className="site-button-small" value="Submit">Add Store</button>
                                </form>
                            </div>
                            <div className="col-xs-7 col-xs-offset-1 area driver-management">
                                <h4>Manage Drivers</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
	return {user: state.user};
}


export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

let DashboardClass = connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default DashboardClass;
