import React , { Component } from "react";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";


export default class Checkout extends Component {

    constructor(props) {
        super();
        this.state = {
            
        };
    }

    componentDidMount() {
        document.title = "Checkout - DeliverMe"
    }

    render() {
        
        return(
            <div className="checkout-page">
                <div className="wrapper">
                    <Navbar />
                    <div className="container">
                        <h3><strong>Checkout</strong></h3>
                    
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}