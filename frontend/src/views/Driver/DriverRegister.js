import React , { Component } from "react";

import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";


export default class DriverRegister extends Component {

    constructor(props) {
        super();
        this.state = {
            
        };
    }

    render() {
        return(
            <div className="driver-page">
                <div className="wrapper">
                    <Navbar />
                    <div className="container main">
                        <h3>Driver Registration</h3>
                        <p>Apply now to become a driver and work your own hours, choose your own deliveries and be your own boss.</p>
                        
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}