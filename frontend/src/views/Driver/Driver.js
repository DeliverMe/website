import React , { Component } from "react";

import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";


export default class Driver extends Component {

    constructor(props) {
        super();
        this.state = {
            
        };
    }

    render() {
        return(
            <div className="driver-page">
                <div className="wrapper">
                    <Navbar />
                    <div className="container main">
                        <h3>Driver</h3>
                        <div className="driver-info">
                            <p>Orders completed 101010101010101</p>
                            <p>Registered Drivers 3883883</p>
                            <p>Areas covered 1010101011</p>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}