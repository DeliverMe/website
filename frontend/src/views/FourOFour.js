import React, {Component} from "react";
import { browserHistory } from "react-router";

export default class FourOFour extends Component {

    componentDidMount() {
        document.title = "Page not found - DeliverMe"
    }

    render() {
        return (
            <div className="FourOFour">
                <img src="/logo.png" alt="logo" />
                <h3>There was an error finding this page.</h3>
                <p>It may not exists or have moved to a new location on the site.</p>
                <button onClick={()=> browserHistory.push("/")} className="site-button">Home</button>
            </div>
        );
    }
}