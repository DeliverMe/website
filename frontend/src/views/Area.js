import React , { Component } from "react";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";

import BigShop from "../components/Shop/BigShop";

export default class Area extends Component {

    constructor(props) {
        super();
        this.state = {
            
        };
    }

    componentDidMount() {
        document.title = this.props.params.where + " - DeliverMe"
    }

    render() {
        const area = this.props.params.where
        return(
            <div className="area-page">
                <div className="wrapper">
                    <Navbar />
                    <div className="container main">
                        <h2><strong>{area}</strong></h2>
                        <h3>has <strong>43</strong> stores delivering <strong>561</strong> items.</h3>
                        <div className="row">
                            <BigShop title="Tesco" location="Dedworth Rd, Windsor, SL4 4JT" image="/shops/tescopic.jpg" waitTime="25" name="Tesco" area="Windsor" />
                            <BigShop offset={true} title="McDonalds" location="Dedworth Rd, Windsor, SL4 4JT" image="/shops/mcdonalds.jpg" waitTime="34" name="McDonalds" area="Windsor"/>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}