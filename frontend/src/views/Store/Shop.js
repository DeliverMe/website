import React, { Component } from "react";
import { browserHistory } from "react-router";
import axios from "axios";

import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";

import Category from "../../components/Shop/Category";
import Item from "../../components/Shop/Item";

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from '../../actions/actionCreators';

class Shop extends Component {

    constructor(props) {
        super();
        this.state = {
            loading: true,
            shop: {},
            items: []
        }
    }

    componentDidMount() {
        const area = this.props.params.where;
        const name = this.props.params.name;
        axios.get(`http://localhost:7033/store/${area}/${name}`).then((response)=> {
            if(response.data.ok === false) {
                browserHistory.push(`/area/${area}`);
            } else {
                document.title = `${response.data.shop.name} - DeliverMe`;
                this.setState({loading: false, shop: response.data.shop, items: response.data.items});
            }
        }).catch((error)=> {
            console.log(error);
            browserHistory.push(`/area/${this.props.params.where}`);
        });
    }

    render() {
        if(this.state.loading ) {
            return (
                <div className="shop-page">
                    <Navbar />
                    <div className="container">
                        <h2>Loading...</h2>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="shop-page">
                    <div className="wrapper">
                        <Navbar />
                        <div className="container">
                            <div className="top">
                                <div className="left">
                                    <h2><strong>{this.state.shop.name} - <span onClick={()=> browserHistory.push("/area/" + this.props.params.where)}>{this.state.shop.location}</span></strong></h2>
                                    <h4>{this.state.shop.address}</h4>
                                    <img src={"/shops/" + this.state.shop.name + "-" + this.state.shop.location + ".jpg"} alt="store header"/>
                                    <div className="categories">
                                        {this.state.shop.categories.split("-").map((category, i)=> (
                                             <Category key={i} type={category} />
                                        ))}
                                    </div>
                                </div>
                                <div className="right">
                                    <h4>{this.state.shop.description}</h4>
                                    <p>Opens: {this.state.shop.open_time}</p>
                                    <p>Closes: {this.state.shop.close_time}</p>
                                    <div className="estimated-time">
                                        <p>Estimated Delivery 34m</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div className="body">
                                <div className="menu">
                                    {this.state.items.map((item, i)=> (
                                        <Item key={i} name={item.name} price={item.price}/>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <Footer />
                </div>
            );
        }
    }
}


function mapStateToProps(state) {
	return {user: state.user};
}


export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

let ShopClass = connect(mapStateToProps, mapDispatchToProps)(Shop);

export default ShopClass;