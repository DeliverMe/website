import React, { Component } from "react";

import Navbar from "../../components/Navbar/Navbar";

export default class StoreAuth extends Component {

    constructor(props) {
        super();
        this.state = {};
    }

    render() {
        return (
            <div className="store-auth">
                <Navbar />
                <div className="holder">
                    <div className="page-area">
                        <div className="top">
                            <h3>DeliverMe <span>|</span> Stores</h3>
                        </div>
                        <div className="body">
                            <p>Store Name</p>
                            <input type="text" name="storename" placeholder="#1578112" id="storename"/>
                            <p>Password</p>
                            <input type="password" name="storepassword" id="storepassword"/>
                            <button className="site-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}