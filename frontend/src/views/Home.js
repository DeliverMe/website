import React, {Component} from 'react';
import { browserHistory } from "react-router";
import axios from "axios";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";

import ModalWrapper from "../components/Modals/ModalWrapper";
import BecomeADriver from "../components/Modals/BecomeADriver";

export default class Home extends Component {

	constructor(props) {
		super();
		this.state = {
			errors: "",
			becomeADriverModal: false,
		};
	}

	componentDidMount() {
        document.title = "Home - DeliverMe"
    }

	areaSubmit(e) {
		//TODO make this verify against the backend.
		e.preventDefault();
		if(this.refs.postcodeInput.value.length === 0) {
			this.setState({errors: "You need to specify a location."});
			return;
		}
		axios.post("http://localhost:7033/order/checkLocation", {location: this.refs.postcodeInput.value}).then((response)=> {
			console.log(response);
            if(response.data.ok === true) {
                browserHistory.push(`/area/${this.refs.postcodeInput.value}`);
            } else {
                this.setState({errors: response.data.error});
            }  
        }).catch((err) => {
			this.setState({errors: "An error occured!"});
			console.log(err);
        });
	}

	changeBecomeADriverModal() {
        this.state.becomeADriverModal  ? this.setState({becomeADriverModal: false}) : this.setState({becomeADriverModal: true});
    }


	render() {
		return (
            <div>
                <div className="home-page">
					<div className="wrapper">
						<Navbar />
						<section className="hero">
							<div className="container info-area">
								<h2>Order <span>food</span> and <span>other items</span> to your door <span>in minutes</span></h2>
								<form onSubmit={(e)=> this.areaSubmit(e)} tabIndex="-1" className="address-form">
									<label htmlFor="postcode">Enter your area or postcode below to search for shops willing to deliver to your address.</label>
									<div className="address-search">
										<input type="text" ref="postcodeInput" name="postcode" id="postcode" placeholder="e.g SL4 6YP or Windsor" autoComplete="postal-code" className="postcode-input"/>
										<input type="submit" id="find_food" value="" className="enter" />
									</div>
									{this.state.errors.length > 0 ? <p className="error">{this.state.errors}</p> : <p className="hide-t">d</p>}
								</form>
								<div className="step-box">
									<div className="step">
										<span className="circle">1</span>
										<p>Enter your <b>postcode </b> or <b>location</b></p>
									</div>
									<div className="step">
										<span className="circle">2</span>
										<p>Choose a <span>restaurant</span> or <span>shop</span> that delivers to your area</p>
									</div>
									<div className="step">
										<span className="circle">3</span>
										<p>Choose your items, place your order and <span> pay</span>, <span> Track</span> your food in real time</p>
									</div>
								</div>
							</div>
						</section>
						<section className="driver-signup">
							<div className="content">
								<h3>Become a driver</h3>
								<p className="desc">DeliverMe drivers are paid good rates and have the freedom to work whenever they want, you could become a driver within a week of signing up for the program.</p>
								<div className="transport-methods">
									<div className="box">
										<i className="fas fa-bicycle" style={{color: "#4286f4"}}></i>
										<p>Bicycles are the best for you and the environment, especially for urban areas.</p>
									</div>
									<div className="box">
										<i className="fas fa-motorcycle" style={{color: "#f4a941"}}></i>
										<p>Motorcylces have the agility of bicycles when choosing your delivery route but dont tire you out.</p>
									</div>
									<div className="box">
										<i className="fas fa-car" style={{color: "#25c650"}}></i>
										<p>Cars allow you to deliver orders from the comfort of your vehicle, especially in the rainy and cold seasons.</p>
									</div>
								</div>
								<button onClick={()=> this.changeBecomeADriverModal()} className="become-a-driver">Register now</button>
								<ModalWrapper changeState={()=> this.changeBecomeADriverModal()} modalIsOpen={this.state.becomeADriverModal} type="become-a-driver-modal">
                                    <BecomeADriver toClose={()=> this.changeBecomeADriverModal()} />
                                </ModalWrapper>

							</div>
						</section>
						<section className="areas">
							<div className="content">
								<h3>We are in 532 towns and cities across the world.</h3>
								
							</div>
						</section>
					</div>
					<Footer />
				</div>
            </div>
        );
    }
}
