import React, { Component } from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import axios from "axios";
import moment from "moment";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import { browserHistory } from "react-router";

class Account extends Component {

    constructor(props) {
        super();
        this.state = {
            loading: true,
            error: "",
            userInfo: {},
            orderInfo: {}
        }
    }

    componentDidMount() {
        if(!this.props.user.loggedIn) {
            browserHistory.push("/");
        } else {
            this.setState({loading: true});
            axios.post("http://localhost:7033/user/accountInformation", {token: this.props.user.token}).then((response)=> {
			    if(response.data.ok === true) {
                    this.setState({userInfo: response.data.user, orderInfo: response.data.orders});
                    this.setState({loading: false});
                } else {
                    this.setState({error: response.data.error});
                    this.setState({loading: false});
                }
            }).catch((err) => {
			    this.setState({error: err});
            });
        }
    }

    render() {
        if(this.state.error.length > 1) {
            return (
                <div className="account-page">
                    <div className="wrapper">
                        <Navbar />
                        <div className="container">
                            <h3>An errror occured loading your account information.</h3>
                            <p>{this.state.error}</p>
                        </div>
                    </div>
                </div>
            );
        }
        if(this.state.loading === false) {
            let createdAt = moment(this.state.userInfo.created).fromNow();
            return (
                <div className="account-page">
                    <div className="wrapper">
                        <Navbar />
                        <div className="container">
                            <h3 className="headline"><strong>{this.state.userInfo.forename + " " + this.state.userInfo.surname}</strong> <span> - Created: {createdAt}</span></h3>
                            <p>Email: {this.state.userInfo.email}</p>
                        </div>
                    </div>
                    <Footer />
                </div>
            );
        } else {
            return (
                <div className="account-page">
                    <div className="wrapper">
                        <Navbar />
                        <div className="container">
                            <h3>Loading...</h3>
                        </div>
                    </div>
                </div>
            );
        }
    }

}

function mapStateToProps(state) {
	return {user: state.user};
}


export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

let AccountClass = connect(mapStateToProps, mapDispatchToProps)(Account);

export default AccountClass;