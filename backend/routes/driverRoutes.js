const express = require("express");
const router = express.Router();
const db = require("../database");
const routesFunctions = require("./routesFunctions");

router.post("/toggleAvailability", async (req, res)=> {
    let connection = await db.getConnection();
    try {
        let token = await routesFunctions.authenticateToken(req, res);
        if (token == null){
            res.status(200).send({ok: false, error: "Token error."});
        }

        if(token.role == "driver") {
            let availability = await connection.query("SELECT available FROM driver_details WHERE user_id = ?", token.user_id);
            let setAvailable;
            availability[0].available == "available" ? setAvailable = "busy" : setAvailable = "available";
            let updateAvailalbe = await connection.query("UPDATE driver_details SET status = ?, last_signed_in = NOW() WHERE user_id =?", [setAvailable, token.user_id]);
            res.status(200).send({ok: true})
        }
    }
    catch(err) {
        res.status(200).send({ok: false});
    }
    finally{
        connection.release();
    }
});

module.exports = router;
