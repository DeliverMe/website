const express = require("express");
const router = express.Router();
const db = require("../database");
const bcrypt = require('bcrypt-nodejs');
const jwt = require("jsonwebtoken");
const config = require("../config/config");
let fs = require("fs");
let ejs = require("ejs");
let nodemailer = require("nodemailer");

//Email transporter
let transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "DeliverMeBot@gmail.com",
        pass: "meowmeowcats"
    }
});

//Standard functions used to handle password hashing.
//Password using the bcrpyt function and is salted 9 times to prevent dictionary attacks.
let generateHash = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}
let validPassword = function(password, hash){
    return bcrypt.compareSync(password, hash);
}

//Function used to register a new user, requires their forename, surname, email, password.
router.post("/register", async (req, res)=> {
    const {forename, surname, email, password} = req.body;

    console.log(__dirname);

    let connection = await db.getConnection();
    try {
        let doesUserExist = await connection.query("SELECT COUNT(*) AS count FROM users WHERE email = ?", email);
        if(doesUserExist[0].count > 0) {
            res.status(200).send({ok: false, error: "A user with that email already exists."});
            return;
        }
        const emailToken = jwt.sign(
            {
                forename, surname, email, password: generateHash(password),
            },
                config.jwtSecret,
            {
                expiresIn: "3h",
            },
        );
        
        
        ejs.renderFile("backend/EmailTemplates/accountconfirmation.ejs", { name: email, token: emailToken }, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                let mainOptions = {
                    from: 'Account Confirmation - DeliverMe',
                    to: email,
                    subject: 'Account Confirmation',
                    html: data
                };
                transporter.sendMail(mainOptions, function (err, info) {
                    if (err) {
                        console.log(err);
                        res.status(200).send({ok: false, error: "An error occured when sending your email."});
                    } else {
                        console.log('Message sent to: ' + email + " info - " + info.response);
                    }
                });
            }
            
        });

        // const url = "http://localhost:7033/user/confirmation/"+emailToken;
        // let mailOptions = {
        //     from: '"DeliverMeBot - Authentication" <DeliverMeBot@gmail.com>',
        //     to: email,
        //     subject: "Account Verification",
        //     html: 'Please use this link to confirm your email: <a href="'+url+'">'+url+'</a>'
        // };
        // await transporter.sendMail(mailOptions, (error, info) => {
        //     if (error) {
        //         res.status(200).send({ok: false, error: "An error occured sending your registration email."});
        //     }
        // });
        res.status(200).send({ok: true, email, role: "user", message: "Awaiting email verification. It may go to your spam box, check that too!"});
    } catch(err) {
        console.log(err);
        res.status(200).send({ok: false, error: "An error occured when registering."});
    } finally {
        connection.release();
    }
});

router.get("/confirmation/:token", async (req, res)=>{
    let connection = await db.getConnection();
    try{
        let emailToken = jwt.verify(req.params.token, config.jwtSecret)
        let doesUserExist = await connection.query("SELECT COUNT(*) AS count FROM users WHERE email = ?", emailToken.email);
        if(doesUserExist[0].count > 0) {
            res.status(200).send({ok: false, error: "User is already verified."});
            return;
        }
        const toInsert = {forename: emailToken.forename, surname: emailToken.surname, email: emailToken.email, password: emailToken.password};
        let insertingUser = await connection.query("INSERT INTO users SET ?", toInsert);
        let userDetails = await connection.query("SELECT role, user_id FROM users WHERE email = ?", emailToken.email);
        let dataJWT = {email: emailToken.email, role: userDetails[0].role, user_id: userDetails[0].user_id, iat: Math.floor(Date.now() / 1000) - 30};
        const token = jwt.sign(dataJWT, config.jwtSecret);
        res.redirect("http://localhost:3000/user-auth/confirmed");
    }
    catch(err){
        console.log(err);
        res.status(200).send({ok: false})
    }
    finally{
        connection.release();
    }
})


router.post("/login", async (req, res)=> {
    const {email, password} = req.body;
    
    let connection = await db.getConnection();
    try {
        //Check if the user exists or not in the database.
        let doesUserExist = await connection.query("SELECT COUNT(*) AS count FROM users WHERE email = ?", email);
        if(doesUserExist[0].count == 0) {
            //They dont exist so they cant login.
            res.status(200).send({ok: false, error: "Username does not exist, or you may have not verified your email if you have just registered your account."});
            return;
        }
        //They do exist, get their details.
        let userDetails = await connection.query("SELECT password, role, user_id, forename, surname FROM users WHERE email = ?", email);
        //They have enetered the wrong password compared to the one stored in the db.
        if (!validPassword(password, userDetails[0].password)){
            res.status(200).send({ok: false, error: "Incorrect password."})
            return;
        }
        //All is good, create the JSON for their token.
        let dataJWT = {email, role: userDetails[0].role, user_id: userDetails[0].user_id, iat: Math.floor(Date.now() / 1000) - 30};
        //Sign the token using the jwt secret in the config file.
        const token = jwt.sign(dataJWT, config.jwtSecret);
        console.log(userDetails);
        res.status(200).send({ok: true, email, token, role: userDetails[0].role, username: userDetails[0].forename + " " + userDetails[0].surname, user_id: userDetails[0].user_id, message: "Login sucessful."});
    } catch(err) {
        res.status(500).send({ok: false, error: "An error occured when logging in. "+err});
    } finally {
        connection.release();
    }
});

router.post("/passwordRecovery", async (req, res)=>{
    try{
        const {user_id, email} = req.body;
        const emailToken = jwt.sign({user_id,},config.jwtSecret,{expiresIn: "1d",},);
        const url = "http://localhost:3000/user/recovery/"+emailToken;
        let mailOptions = {
            from: '"DeliverMeBot" <DeliverMeBot@gmail.com>',
            to: email,
            subject: "Password Change",
            html: 'Please use this link to change your password: <a href="'+url+'">'+url+'</a>'
        };

        //Send the email to the user.
        await transporter.sendMail(mailOptions, (error, info) => {
            if (err) {
                return console.log(err);
            }
            console.log('Message sent: %s', info.messageId);
        });
        res.status(200).send({ok: true, email, message: "Awaiting email verification."});
        }
    catch(err){
        console.log(err)
        res.status(200).send({ok: false, error: "An error occured when trying to send email."});
    }
    finally{
        return;
    }
});

router.post("/passwordChange", async (req, res)=>{
    const {user_id, password} = req.body;
    let connection = await db.getConnection();
    try{
        let updatePassword = await connection.query("UPDATE users SET password = ? WHERE user_id = ?", [generateHash(password), user_id])
        res.status(200).send({ok: true, message: "Password sucessfully changed."})
    }
    catch(err){
        console.log(err);
        res.status(200).send({ok: false, error: "Could not change password."})
    }
    finally{
        connection.release();
    }
});

//TODO - Make this route require authentication. Would get the token from the req.auth rather than a body object.
router.post("/accountInformation", async (req, res)=> {
    let connection = await db.getConnection();
    try {
        const auth = req.body.token;
        jwt.verify(auth, config.jwtSecret, async function (err, decoded) {
            if (err) {
                res.status(200).send({ok: false, error: "Error verifying your token"});
                return;
            }
            //everything is fine with the token, get the user details.
            let userAccountInfo = await connection.query("SELECT user_id, created, email, forename, surname, role FROM users WHERE email = ?", [decoded.email]); //THE FOLLOWING 2 QUERIES SHOULD BE OPTIMIZED INTO ONE QUERY USING POSSIBLY A JOIN.
            let userOrderInfo = await connection.query("SELECT * FROM orders WHERE ordered_by = ?", [userAccountInfo[0].ordered_by]);
            res.status(200).send({ok: true, user: userAccountInfo[0], orders: userOrderInfo});
        });
    } catch (err) {
        res.status(200).send({ok: false, error: err});
    } finally {
        connection.release();
    }
});
module.exports = router;
