const express = require("express");
const router = express.Router();
const db = require("../database");
const config = require("../config/config");
const routesFunctions = require("./routesFunctions");

router.post("/makeDriver", async (req, res)=>{
    const {user_id} = req.body;
    let token = await routesFunctions.authenticateToken(req, res);

    if (token == null){
        res.status(200).send({ok: false, error: "Acess denied."});
    }

    let connection = await db.getConnection();
    try{
        if (token.role == "admin"){
            let updateRole = await connection.query("Update users SET role = 'driver' WHERE user_id = ?", user_id);
            let insertDriverDetails = await connection.query("INSERT INTO driver_details SET user_id = ?", user_id);
            res.status(200).send({ok: true});
        }
    }
    catch(err){
        res.status(200).send({ok: false, error: err});
    }
    finally{
        connection.release();
    }
});

router.post("/verifyUser", config.jwtAuthAdmin, async (req, res)=> {
    const auth = req.get("authorization") || req.get("Authorization");
    res.status(200).send({ok: true, message: "You are an admin."});
});

router.get("/getAllUsers", config.jwtAuthAdmin, async (req, res)=> {
    let connection = await db.getConnection();
    try {
        let allUsers = await connection.query("SELECT user_id, forename, surname, email, role FROM users ORDER BY created DESC LIMIT 50");
        res.status(200).send({ok: true, users: allUsers});
    } catch (err) {
        res.status(200).send({ok: false, error: err});
    } finally {
        connection.release();
    }
});

module.exports = router;
