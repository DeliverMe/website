const express = require("express");
const router = express.Router();
const db = require("../database");
const nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "DeliverMeBot@gmail.com",
        pass: "meowmeowcats"
    }
});
// todo:
//make /checkStock, /completeOrder, /assignDriver


router.post("/checkLocation", async (req, res)=>{
    let { location } = req.body;
    const connection = await db.getConnection();
    try{
        let checkLocation = await connection.query("SELECT COUNT(*) AS count FROM stores WHERE location = ?", location);
        if(checkLocation[0].count > 0){
            res.status(200).send({ok: true, message: "Location exists"});
        } else {
            res.status(200).send({ok: false, error: "That location has no stores on our service, try again another time."});
        }
    }
    catch(err) {
        res.status(200).send({ok: false, error: "Error occured checking databse."})
    }
    finally {
        connection.release();
    }
});

router.post("/makeOrder", async (req, res)=>{
    let {user_id, items, store} = req.body;
    const connection = await db.getConnection();
    try{
        let checkOrders = await connection.query("SELECT COUNT(*) AS count FROM orders WHERE ordered_by = ?", user_id);
        if(checkOrders[0].count > 0){
            res.status(200).send({ok: false, error: "User already has an ongoing order."});
            return;
        }
        let price = 2.5;
        items = JSON.parse(items)
        for(let key in items){
            let item = items[key];
            let getPrice = await connection.query("SELECT price FROM items WHERE item_id = ?", item.item_id);
            price += getPrice[0].price * item.quantity
        }
        let addOrder = await connection.query("INSERT INTO orders SET ?", {ordered_by: user_id, price});
        let order_id = await connection.query("SELECT order_id FROM orders WHERE ordered_by = ?", user_id)
        for(let key in items){
            let item = items[key];
            let addItem = await connection.query("INSERT INTO ordered_items SET ?", {item_id: item.item_id, order_id: order_id[0].order_id, quantity: item.quantity})
            let getStock = await connection.query("SELECT stock FROM items WHERE item_id = ?", item.item_id)
            let removeStock = await connection.query("UPDATE items SET stock = ? WHERE item_id = ?", [getStock[0].stock - item.quantity, item.item_id])
            res.status(200).send({ok: true, message: "Order has been added to the database."})
        }
    }
    catch(err){
        console.log(err)
        res.status(200).send({ok: false, error: "Problem making order."})
    }
    finally{
        connection.release();
    }
});

/** HOW THE ORDER SYSTEM WILL WORK.
 * 1) User requests an order, they will then have their order set to the first status, pending.
 * 2) The backend should then decide what driver to allocate to the job, this should be based on number of items (cars more suitable for big orders), how long will it take to get to the customer (if the order wont be ready for 5 mins you should send the driver 5 mins away from the shop to the order not the one outside as this will keep drivers feeling like they are doing something.)
 * 3) The driver will then accept the order, update the order status to preping. You may want to put the stage proccess in the driver routes or order routes up to you.
 * 4) The driver will then go to the shop, pick up the order and set their status to delivering or whatever number 3 is. This is when you will track your driver as a customer on the map, will need to figure that out.
 * 5) It then turns up and tada. Once an order is created though you will be receiving a customer request every 5 seconds to the backend for an uodate, you may want to store the status in an array and update that when the driver updates the order status. This will reduce the number of daabase requests needed, which may be excessive.
 * 
 * Maps will be done through google maps i guess, you could play around with that to help determine delivery time estimates and tracking locations etc...
 * 
 */

module.exports = router;
