const express = require("express");
const router = express.Router();
const db = require("../database");

router.get("/:area/:shop", async (req, res)=> {
    const { area, shop } = req.params;
    let connection = await db.getConnection();
    try {
        let storeData = await connection.query("SELECT * FROM stores WHERE location = ? AND name = ?", [area, shop]);
        //NEED TO ALSO DETERMINE IF THE SHOP IS CURRENTLY OPEN, IF SO SEND A CLOSES_IN DATA WITH A RELATIVE TIME UNTIL IT CLOSES USING MOMENT.JS. IF THE SHOP IS CLOSED DO THE SAME BUT UNTIL IT OPENS.
        //TODO ALSO RETURN MENU ITEMS FOR THIS STORE.
        if(storeData.length == 0) {
            res.status(200).send({ok: false, error: "No stores in " + area + " with name " + shop + " exist."});
        } else {
            let menuData = await connection.query("SELECT item_id, name, description, price, category FROM items WHERE stock > 0 AND store_id = ?", [storeData[0].store_id]);
            res.status(200).send({ok: true, shop: storeData[0], items: menuData});
        }
    } catch (err) {
        res.status(200).send({ok: false, error: err})
    } finally {
        connection.release();
    }
});

module.exports = router;
