const express = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

//Function used to verify a JWT token is valid on the system. Uses the JWT secret in the config file.
async function authenticateToken(req, res){
    //Get the auth header from the user
    const auth = req.get("authorization") || req.get("Authorization");

    //Check if the auth exists, if not reject
    if(!auth || typeof auth === "undefined") {
        res.status(200).send({ok: false, error: "Invalid auth token"});
        return;
    }

    //Auth tokens come in the form Bearer <TOKEN HERE> from the user, we need to split at the space and only use the <TOKEN HERE> part.
    let token = auth.split(" ")[1];
    return await jwt.verify(token, config.jwtSecret);
}


module.exports = {authenticateToken};
