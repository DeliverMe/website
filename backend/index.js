const express = require("express");
const http = require("http");
const bodyParser = require("body-parser");
const cors = require("cors");

let app = express();
const port = 7033;

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

//File imports
const adminRoutes = require("./routes/adminRoutes");
const driverRoutes = require("./routes/driverRoutes");
const orderRoutes = require("./routes/orderRoutes");
const userRoutes = require("./routes/userRoutes");
const storeRoutes = require("./routes/storeRoutes");


//Setup rest api
app.use("/admin", adminRoutes);
app.use("/driver", driverRoutes);
app.use("/order", orderRoutes);
app.use("/user", userRoutes);
app.use("/store", storeRoutes);


// 404 Error Handler
const endpointError = {status: 404, error: "No Endpoint Found"}
app.use((req, res) => {
    res.status(404).send(endpointError);
});


process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});

// app.get("/confirmation/:token", async (req, res)=>{
//     try{
//         let emailToken = jwt.verify(req.params.token, config.jwtSecret)
//         const toInsert = {forename: emailToken.forename, surname: emailToken.surname, email: emailToken.email, password: emailToken.password};
//         let insertingUser = await connection.query("INSERT INTO users SET ?", toInsert);
//         let userDetails = await connection.query("SELECT role, user_id FROM users WHERE email = ?", email);
//         let dataJWT = {email, role: userDetails[0].role, user_id: userDetails[0].user_id, iat: Math.floor(Date.now() / 1000) - 30};
//         const token = jwt.sign(dataJWT, config.jwtSecret);
//     }
//     catch(err){
//         console.log(err);
//         res.status(200).send({ok: false})
//     }
//     finally{
//         return res.redirect("http://localhost:3001/login");
//     }
// })

//Create the http server.
let server = http.createServer(app);
server.listen(port);
console.log("The server has started on port " + port);
module.exports = app;