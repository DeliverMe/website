-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.1-dmr-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table deliverme.driver_details
CREATE TABLE IF NOT EXISTS `driver_details` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `started` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When they upgraded to a driver.',
  `last_signed_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`driver_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.driver_details: ~-1 rows (approximately)
/*!40000 ALTER TABLE `driver_details` DISABLE KEYS */;
INSERT INTO `driver_details` (`driver_id`, `user_id`, `available`, `started`, `last_signed_in`) VALUES
	(1, 2, 1, '2018-09-06 01:57:31', '2018-09-06 18:12:11'),
	(2, 3, 1, '2018-09-06 17:05:45', '2018-09-06 18:07:16'),
	(3, 5, 0, '2018-09-06 19:06:44', '2018-09-06 19:06:44');
/*!40000 ALTER TABLE `driver_details` ENABLE KEYS */;

-- Dumping structure for table deliverme.items
CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `description` varchar(140) NOT NULL DEFAULT 'Description.',
  `stock` int(11) NOT NULL,
  `category` varchar(12) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `FK_items_stores` (`store_id`),
  CONSTRAINT `FK_items_stores` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.items: ~-1 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`item_id`, `store_id`, `name`, `price`, `description`, `stock`, `category`) VALUES
	(1, 3, 'Beans', 2.99, 'A can of beans, full of fibre.', 69, 'Food'),
	(2, 3, 'Brown Bread', 2.00, 'A freshly baked loaf of brown bread', 21, 'Food'),
	(3, 4, '1/4 Pounder', 6.50, 'A burger meal that comes with cheese, ketchip, pickles and a drink', 100, 'Food');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table deliverme.ordered_items
CREATE TABLE IF NOT EXISTS `ordered_items` (
  `ordered_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ordered_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.ordered_items: ~-1 rows (approximately)
/*!40000 ALTER TABLE `ordered_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordered_items` ENABLE KEYS */;

-- Dumping structure for table deliverme.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordered_by` int(11) NOT NULL,
  `driver` int(11) NOT NULL,
  `price` float NOT NULL,
  `time_ordered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('pending','preping','delivering','finished') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.orders: ~-1 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table deliverme.stores
CREATE TABLE IF NOT EXISTS `stores` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `location` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `categories` varchar(30) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `type` enum('shop','restaurant') NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `description` varchar(200) NOT NULL DEFAULT 'Desciption',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.stores: ~-1 rows (approximately)
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` (`store_id`, `name`, `location`, `address`, `categories`, `image`, `type`, `open_time`, `close_time`, `description`) VALUES
	(3, 'Tesco', 'Windsor', '12 Bilal Road', 'General-Food-Alcohol', 'meow', 'shop', '12:34:58', '16:34:59', 'This is the small tesco not the big one.'),
	(4, 'McDonalds', 'Windsor', '34 Sheep Way', 'Food', 'meow', 'restaurant', '16:39:58', '23:34:59', 'The McDonalds on the hill near the castle.');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;

-- Dumping structure for table deliverme.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `forename` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(70) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` enum('user','driver','admin') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table deliverme.users: ~-1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `forename`, `surname`, `email`, `password`, `created`, `role`) VALUES
	(1, 'Jasper', 'Morgan', 'Jmorgan@mail.com', 'test', '2018-09-05 23:41:59', 'user'),
	(2, 'Tommy', 'Turner', 'TommyT@gmail.com', '$2a$09$E7Fv2dM7T4M8P/.fDnhVjuVsHYUCqggcViaaNWpIlY6MlNoaQGD2a', '2018-09-06 00:26:04', 'driver'),
	(3, 'Tommy', 'Turner', 'TommyT2@gmail.com', '$2a$09$We1t0HV6wm2FreISrC.6s.xpJbfkBp2GKnuoTXvzWYBhPPyPalar.', '2018-09-06 00:26:52', 'driver'),
	(4, 'Tommy', 'Turner', 'TommyT3@gmail.com', '$2a$09$8xluPErsgKjrYED1GQ0A1ubW/VE6ZAraqN4ZuTcOqK3kTYYvY1wnG', '2018-09-06 00:27:58', 'admin'),
	(5, 'Tommy', 'Turner', 'TommyT4@gmail.com', '$2a$09$lfRAiokXpiUUOk67eXHmseZ6hLoAxVGBlOhmDYh5BYS4UxZzEfcvm', '2018-09-06 00:29:15', 'driver'),
	(10, 'Tom', 'Tomm', 'TommyT5@gmail.com', '$2a$09$vgpXwSW8mhmPrf1HZfwnc.3Egq2aEt3zREscPoQaNkU5jF/o8If8W', '2018-09-06 18:38:38', 'user'),
	(21, 'Billy', 'Elderfield', 'billbobaginsxbox@live.co.uk', '$2a$09$v0DwdmtkoR9bYes.vUx9HeD5sgdM0YTudPvWZkUVQCgzR3PvH8gEq', '2018-09-07 02:12:04', 'user'),
	(28, 'Admin', 'Admin', 'thomasisthebest111@gmail.com', '$2a$09$dIw1mjrz0U2BJydKGxq4oOCiA1/tPRXZLLx3CczAA3MAOwQE1Ddh6', '2018-09-16 00:21:55', 'admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
