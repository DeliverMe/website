const jwt = require("jsonwebtoken");

//The secret used in the signing and reading of json web tokens.
const jwtSecret = "justarandomsignaturethingdontcopyitbecauseidontactuallyuseit.";

//A function to verify the person making the action is off role teacher or admin.
async function jwtAuthAdmin(req, res, next) {
    const auth = req.get("authorization") || req.get("Authorization");

    if(!auth || typeof auth === "undefined") {
        res.status(200).send({ok: false, error: "Invalid auth token"});
        return;
    }

    const token = auth.split(" ")[1]; //come in the form Bearer TOKENHERE, we only want the TOKENHERE bit.
    try {
        jwt.verify(token, jwtSecret, function(err, decoded) {
            if (err) {
                res.status(200).send({ok: false, error: "Error verifying your token"});
                return;
            }

            decoded.role === "admin" ? next() : res.status(200).send({ok: false, error: "Insufficient permissions"});
        });
    } catch (err) {
        res.status(200).send({ok: false, error: "Auth token processing error"});
    }
};


module.exports = {jwtSecret, jwtAuthAdmin};